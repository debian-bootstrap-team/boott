#!/bin/sh

file=$1
index=$2
package=$3
# dscverify could be extended to support checking Packages files as well
if [ "$(md5sum "$file" | awk '{print $1}')" != "$(grep-dctrl -X -F Package "$package" -s MD5sum -n "$index" | awk '{print $1}')" ]; then
	echo "md5sum mismatch for $file"
	exit 1
fi
if [ "$(wc -c < "$file")" != "$(grep-dctrl -X -F Package "$package" -s Size -n "$index")" ]; then
	echo "size mismatch for $file"
	exit 1
fi
