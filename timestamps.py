#!/usr/bin/env python3

from datetime import date, timedelta as t
last = date.today()
i = date(2005, 3, 12)
while (i <= last):
    print(i.strftime("%Y%m%d"))
    i += t(5)
