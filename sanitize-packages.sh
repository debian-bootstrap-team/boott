#!/bin/sh
# this script sanitizes debian Packages files from snapshot.debian.org

date="$1"
packages="$2"

# version numbers cannot contain special characters like this:
# Replaces: icedove (<< 2.0~Â)
if [ 20070605 -le $date ] && [ $date -le 20070615 ]; then
	sed -i 's/Replaces: icedove (<< 2.0\x7e\xc2)/Replaces: icedove (<< 2.0)/' "$packages"
fi

# version numbers cannot contain braces and dollar signs like this:
# Depends: syncekonnector (= ${Source-Version])
if [ 20060322 -le $date ] && [ $date -le 20060327 ]; then
	sed -i 's/syncekonnector (= ${Source-Version]),/syncekonnector,/' "$packages"
fi

# version numbers cannot be only plus signs like this:
# libghc6-hsql-dev (<< ++)
if [ 20060814 -le $date ] && [ $date -le 20060824 ]; then
	sed -i 's/libghc6-hsql-dev (<< ++),/libghc6-hsql-dev,/' "$packages"
fi

# version numbers cannot be <none> or <none>+~ like this:
# python-sip4 (>= <none>), python-sip4 (<< <none>+~)
if [ 20100224 -le $date ] && [ $date -le 20100505 ]; then
	sed -i 's/python-sip4 (>= <none>), python-sip4 (<< <none>+~)/python-sip4/' "$packages"
fi

# lsparisc binary exists but the according source package is of different
# version and only buildable on hppa - so remove lsparisc
if [ 20060605 -le $date ] && [ $date -le 20080210 ]; then
	tmpfile=$(mktemp)
	grep-dctrl --not --exact-match -FPackage lsparisc "$packages" > "$tmpfile"
	mv "$tmpfile" "$packages"
fi
