#!/usr/bin/env python3

import sys
import os

# remove the path of the current module from PYTHONPATH
if os.path.dirname(os.path.dirname(__file__)) in sys.path:
    sys.path.remove(os.path.dirname(os.path.dirname(__file__)))

# backup the current values of the modules cache
# these are our current dummy module
oldmod = sys.modules["urllib"]
oldmod2 = sys.modules["urllib.parse"]

# remove the cache entries so that they are loaded anew
del sys.modules["urllib"]
del sys.modules["urllib.parse"]

# import again, this time the real library
import urllib.parse
import ssl

# implement urlopen with a SSL context that doesn't check the cert
def quote(*a, **k):
    return urllib.parse.quote(*a, **k)

def unquote(*a, **k):
    return urllib.parse.unquote(*a, **k)

def urlsplit(*a, **k):
    return urllib.parse.urlsplit(*a, **k)

def urlparse(*a, **k):
    return urllib.parse.urlparse(*a, **k)

def urlsplit(*a, **k):
    return urllib.parse.urlsplit(s, **k)

def urljoin(*a, **k):
    return urllib.parse.urljoin(*a, **k)
def unwrap(*a, **k):
    return urllib.parse.unwrap(*a, **k)
def splittype(*a, **k):
    return urllib.parse.splittype(*a, **k)
def splithost(*a, **k):
    return urllib.parse.splithost(*a, **k)
def splitport(*a, **k):
    return urllib.parse.splitport(*a, **k)
def splituser(*a, **k):
    return urllib.parse.splituser(*a, **k)
def splitpasswd(*a, **k):
    return urllib.parse.splitpasswd(*a, **k)
def splitattr(*a, **k):
    return urllib.parse.splitattr(*a, **k)
def splitquery(*a, **k):
    return urllib.parse.splitquery(*a, **k)
def splitvalue(*a, **k):
    return urllib.parse.splitvalue(*a, **k)
def splittag(*a, **k):
    return urllib.parse.splittag(*a, **k)
def to_bytes(*a, **k):
    return urllib.parse.to_bytes(*a, **k)
def unquote_to_bytes(*a, **k):
    return urllib.parse.unquote_to_bytes(*a, **k)
def urlunparse(*a, **k):
    return urllib.parse.urlunparse(*a, **k)


# restore the cache so that the code importing this module uses
# it and not the real urllib
sys.modules["urllib"] = oldmod
sys.modules["urllib.parse"] = oldmod2
