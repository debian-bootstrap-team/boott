#!/usr/bin/env python3

import sys
import os

# remove the path of the current module from PYTHONPATH
if os.path.dirname(os.path.dirname(__file__)) in sys.path:
    sys.path.remove(os.path.dirname(os.path.dirname(__file__)))

# backup the current values of the modules cache
# these are our current dummy module
oldmod = sys.modules["urllib"]
oldmod2 = sys.modules["urllib.request"]

# remove the cache entries so that they are loaded anew
del sys.modules["urllib"]
del sys.modules["urllib.request"]

# import again, this time the real library
import urllib.request
import ssl

# implement urlopen with a SSL context that doesn't check the cert
def urlopen(u):
    context = ssl.create_default_context()
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE
    return urllib.request.urlopen(u, context=context)

# restore the cache so that the code importing this module uses
# it and not the real urllib
sys.modules["urllib"] = oldmod
sys.modules["urllib.request"] = oldmod2
