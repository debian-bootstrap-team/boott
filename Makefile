V=1

# When a recipe redirects the output of a command to a file, then this
# (probably empty) file will appear as up-to-date to make even if the command
# failed. To prevent this, we want to always delete the target of failed rules.
.DELETE_ON_ERROR:
# Behave like POSIX. In particular, if this target is mentioned then recipes
# will be invoked as if the shell had been passed the -e flag: the first
# failing command in a recipe will cause the recipe to fail immediately.
.POSIX:

MIRROR:=http://snapshot.debian.org/archive
MIRRORDEB:=http://httpredir.debian.org/debian
# We are interested either in the this Monday 00:00 (in case it is Monday) or
# in the last Monday 00:00 (in case it is not Monday). We do that by adding one
# second to the epoch of last Sunday at 23:59:59.
# Since leap seconds are not counted, adding one second is sufficient.
EPOCH=$(shell echo $$(($$(TZ=0 date -d "last sunday 23:59:59 UTC" "+%s")+1)))
SNAPSHOT:=$(shell TZ=0 date -d "@$(EPOCH)" "+%Y%m%dT%H%M%SZ")

# We list cross_all.html as the first target so that during a parallel make it
# gets executed first and so that the targets that take less time can "fill up"
# the remaining cores. If cross_all.html is listed last, it would just end up
# being executed by itself at the end without other processes in parallel.
#TARGETS:=cross_all.html cross_matrix.html foreign_install.html co_ma_same.html ma_interpreter.html essential.html importance_metric_all.txt
TARGETS:=cross_all.html foreign_install.html co_ma_same.html ma_interpreter.html essential.html history.svg history_cross.svg botch-native/amd64/stats.html
export PYTHONPATH:=./py:$(PYTHONPATH)

.PHONY: all
all: $(TARGETS)

# target that doesn't exist so that targets depending on it are always rebuild
.PHONY: nonexistent
nonexistent:

thisweek: nonexistent
	TZ=0 touch --date="@$(EPOCH)" thisweek

##
# includes and defines
##

# a function that allows checking for string equality
# the first condition checks for the special case where both strings are empty
eq = $(if $(or $(1),$(2)),$(and $(findstring $(1),$(2)),\
                                $(findstring $(2),$(1))),1)

-include mainarchs.make
-include portarchs.make

PACKAGES_MAIN:=$(foreach arch,$(MAINARCHS),Packages_main_$(arch).xz)
PACKAGES_MAIN_UNPACKED:=$(foreach arch,$(MAINARCHS),Packages_main_$(arch))
PACKAGES_MAIN_NOALL_NOMAFOREIGN:=$(foreach arch,$(MAINARCHS),Packages_main_$(arch)_noall_nomaforeign)
PACKAGES_PORTS:=$(foreach arch,$(PORTARCHS),Packages_ports_$(arch).gz)
PACKAGES_PORTS_UNPACKED:=$(foreach arch,$(PORTARCHS),Packages_ports_$(arch))
PACKAGES_PORTS_NOALL_NOMAFOREIGN:=$(foreach arch,$(PORTARCHS),Packages_ports_$(arch)_noall_nomaforeign)
PACKAGES_UNRELEASED:=$(foreach arch,$(PORTARCHS),Packages_unreleased_$(arch).gz)
PACKAGES_UNRELEASED_UNPACKED:=$(foreach arch,$(PORTARCHS),Packages_unreleased_$(arch))
PACKAGES_UNRELEASED_NOALL_NOMAFOREIGN:=$(foreach arch,$(PORTARCHS),Packages_unreleased_$(arch)_noall_nomaforeign)

# the empty lines at the end are important or otherwise everything will be in a
# single line if called from within a foreach
define BUILDCHECK_MAIN_MAIN_PREREQ
MATRIX_$1_$2_PREREQ:=Packages_main_$1 Packages_main_$2_noall_nomaforeign

endef
define BUILDCHECK_MAIN_PORT_PREREQ
MATRIX_$1_$2_PREREQ:=Packages_main_$1 Packages_ports_$2_noall_nomaforeign Packages_unreleased_$2_noall_nomaforeign

endef
define BUILDCHECK_PORT_MAIN_PREREQ
MATRIX_$1_$2_PREREQ:=Packages_ports_$1 Packages_unreleased_$1 Packages_main_$2_noall_nomaforeign

endef
define BUILDCHECK_PORT_PORT_PREREQ
MATRIX_$1_$2_PREREQ:=Packages_ports_$1 Packages_unreleased_$1 Packages_ports_$2_noall_nomaforeign Packages_unreleased_$2_noall_nomaforeign

endef

# No secondary expansion is needed for $(MATRIX_$1_$2_PREREQ) because the
# define is eval()-ed after the variables have been set
define BUILDCHECK_MATRIX
builddebcheck-$1-$2.yaml: Sources_noall crossbuild-essential-$1-$2 $(MATRIX_$1_$2_PREREQ) $3 $4 $5 $6
	dose-builddebcheck --latest 1 --deb-drop-b-d-indep \
		--deb-profiles=cross,nocheck \
		--explain --failures \
		--deb-native-arch=$1 --deb-host-arch=$2 \
		--outfile=builddebcheck-$1-$2.yaml \
		crossbuild-essential-$1-$2 $3 $4 $5 $6 \
		Sources_noall \
		|| [ "$$$$?" -lt 64 ]

builddebcheck-$1-$2.html: builddebcheck-$1-$2.yaml $3 $4 $5 $6
	$(eval DESCRIPTION=<p>Below are two tables showing the two problem classes that \
		prevent cross compilation on the dependency level. \
		This example tries to satisfy the crossbuild dependencies of \
		all source packages on $1 as the build architecture for $2 \
		as the host architecture in current Debian sid.</p> \
		<p>A machine parsable version can be retrieved in \
		<a href="builddebcheck-$1-$2.yaml">dose yaml format</a></p> \
	) \
	/usr/bin/time -v botch-dose2html --wnpp --btsuser debian-cross@lists.debian.org \
		--btstag cross-satisfiability --desc '$(DESCRIPTION)' --timestamp "$(SNAPSHOT)" \
		--srcsdir cross_matrix_$1_$2 --packages $3 --packages $4 --packages $5 --packages $6 --wwwroot=. \
		builddebcheck-$1-$2.yaml builddebcheck-$1-$2.html

endef

define CROSSBUILD_ESSENTIAL
crossbuild-essential-$1-$2:
	echo > crossbuild-essential-$1-$2
	echo Package: crossbuild-essential-$2 >> crossbuild-essential-$1-$2
	echo Architecture: $1 >> crossbuild-essential-$1-$2
	echo Depends: libc-dev:$2, libstdc++-dev:$2 >> crossbuild-essential-$1-$2
	echo Version: 1 >> crossbuild-essential-$1-$2

endef

# this include must come *after* the defines for BUILDCHECK_MATRIX and CROSSBUILD_ESSENTIAL
-include matrix.make

##
# generated makefiles
##

mainarchs.make: mainarchs.txt Makefile
	echo MAINARCHS:=$(shell cat mainarchs.txt) > mainarchs.make

portarchs.make: portarchs.txt Makefile
	echo PORTARCHS:=$(shell cat portarchs.txt) > portarchs.make

matrix.make: Makefile
	$(file > matrix.make,) \
	$(file >> matrix.make, \
		MATRIX_YAML:=$$(foreach B,$$(MAINARCHS) $$(PORTARCHS), \
			$$(foreach H,$$(MAINARCHS) $$(PORTARCHS), \
				builddebcheck-$$B-$$H.yaml))) \
	$(file >> matrix.make, \
		MATRIX_HTML:=$$(foreach B,$$(MAINARCHS) $$(PORTARCHS), \
			$$(foreach H,$$(MAINARCHS) $$(PORTARCHS), \
				builddebcheck-$$B-$$H.html))) \
	$(file >> matrix.make, \
		MATRIX_PREREQ:=$$(foreach B,$$(MAINARCHS) $$(PORTARCHS), \
			$$(foreach H,$$(MAINARCHS) $$(PORTARCHS), \
				crossbuild-essential-$$B-$$H))) \
	$(foreach I,MAIN PORT,$(foreach J,MAIN PORT, \
		$(file >> matrix.make, \
			$$(eval $$(foreach B,$$($(I)ARCHS), \
				$$(foreach H,$$($(J)ARCHS), \
					$$(call BUILDCHECK_$(I)_$(J)_PREREQ,$$B,$$H))))))) \
	$(eval MAIN_PKGSB=Packages_main_$$$$B,/dev/null) \
	$(eval MAIN_PKGSH=Packages_main_$$$$H_noall_nomaforeign,/dev/null) \
	$(eval PORT_PKGSB=Packages_ports_$$$$B,Packages_unreleased_$$$$B) \
	$(eval PORT_PKGSH=Packages_ports_$$$$H_noall_nomaforeign,Packages_unreleased_$$$$H_noall_nomaforeign) \
	$(foreach I,MAIN PORT,$(foreach J,MAIN PORT, \
		$(file >> matrix.make, \
			$$(eval $$(foreach B,$$($(I)ARCHS), \
				$$(foreach H,$$($(J)ARCHS), \
					$$(call BUILDCHECK_MATRIX,$$B,$$H,$($(I)_PKGSB),$($(J)_PKGSH)))))))) \
	$(foreach I,MAIN PORT,$(foreach J,MAIN PORT, \
		$(file >> matrix.make, \
			$$(eval $$(foreach B,$$($(I)ARCHS), \
				$$(foreach H,$$($(J)ARCHS), \
					$$(call CROSSBUILD_ESSENTIAL,$$B,$$H))))))) \

##
# curl-based targets
##

release: release.gpg
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output release $(MIRROR)/debian/$(SNAPSHOT)/dists/sid/Release
	TZ=0 touch --date="@$(EPOCH)" release
	gpgv $(shell for f in /etc/apt/trusted.gpg.d/debian-archive-*-automatic.gpg; do echo --keyring=$$f; done) \
		release.gpg release

release.gpg: thisweek
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output release.gpg $(MIRROR)/debian/$(SNAPSHOT)/dists/sid/Release.gpg
	TZ=0 touch --date="@$(EPOCH)" release.gpg

ports-release: ports-release.gpg debian-ports-archive-keyring.gpg
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output ports-release $(MIRROR)/debian-ports/$(SNAPSHOT)/dists/sid/Release
	TZ=0 touch --date="@$(EPOCH)" ports-release
	gpgv --keyring=./debian-ports-archive-keyring.gpg ports-release.gpg ports-release

ports-release.gpg: thisweek
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output ports-release.gpg $(MIRROR)/debian-ports/$(SNAPSHOT)/dists/sid/Release.gpg
	TZ=0 touch --date="@$(EPOCH)" ports-release.gpg

unreleased-release: unreleased-release.gpg debian-ports-archive-keyring.gpg
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output unreleased-release $(MIRROR)/debian-ports/$(SNAPSHOT)/dists/unreleased/Release
	TZ=0 touch --date="@$(EPOCH)" unreleased-release
	gpgv --keyring=./debian-ports-archive-keyring.gpg unreleased-release.gpg unreleased-release

unreleased-release.gpg: thisweek
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output unreleased-release.gpg $(MIRROR)/debian-ports/$(SNAPSHOT)/dists/unreleased/Release.gpg
	TZ=0 touch --date="@$(EPOCH)" unreleased-release.gpg

Sources.xz: release
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output Sources.xz $(MIRROR)/debian/$(SNAPSHOT)/dists/sid/main/source/Sources.xz
	TZ=0 touch --date="@$(EPOCH)" Sources.xz
	./checkmd5.sh Sources.xz release main/source/Sources.xz

Packages_main_%.xz: release
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output Packages_main_$(*F).xz $(MIRROR)/debian/$(SNAPSHOT)/dists/sid/main/binary-$(*F)/Packages.xz
	TZ=0 touch --date="@$(EPOCH)" Packages_main_$(*F).xz
	./checkmd5.sh Packages_main_$(*F).xz release main/binary-$(*F)/Packages.xz

Packages_ports_%.gz: ports-release
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output Packages_ports_$(*F).gz $(MIRROR)/debian-ports/$(SNAPSHOT)/dists/sid/main/binary-$(*F)/Packages.gz
	TZ=0 touch --date="@$(EPOCH)" Packages_ports_$(*F).gz
	./checkmd5.sh Packages_ports_$(*F).gz ports-release main/binary-$(*F)/Packages.gz

Packages_unreleased_%.gz: unreleased-release
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output Packages_unreleased_$(*F).gz $(MIRROR)/debian-ports/$(SNAPSHOT)/dists/unreleased/main/binary-$(*F)/Packages.gz
	TZ=0 touch --date="@$(EPOCH)" Packages_unreleased_$(*F).gz
	./checkmd5.sh Packages_unreleased_$(*F).gz unreleased-release main/binary-$(*F)/Packages.gz

debian-ports-archive-keyring_2018.12.27_all.deb: Packages_main_amd64
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --remote-time --output debian-ports-archive-keyring_2018.12.27_all.deb http://ftp.debian.org/debian/pool/main/d/debian-ports-archive-keyring/debian-ports-archive-keyring_2018.12.27_all.deb
	./checkmd5_pkg.sh debian-ports-archive-keyring_2018.12.27_all.deb Packages_main_amd64 debian-ports-archive-keyring

##
#
##

mainarchs.txt: release
	grep-dctrl '' -n -s Architectures release | tr ' ' '\n' | grep -v '^all$$' > mainarchs.txt || [ "$$?" -eq 1 ]

portarchs.txt: ports-release
	grep-dctrl '' -n -s Architectures ports-release | tr ' ' '\n' | grep -v '^all$$' > portarchs.txt || [ "$$?" -eq 1 ]

Packages_main_%: Packages_main_%.xz
	xz --decompress --stdout Packages_main_$(*F).xz > Packages_main_$(*F)

Sources: Sources.xz
	xz --decompress --stdout Sources.xz > Sources

Packages_%_noall_nomaforeign: Packages_%
	grep-dctrl -X \( --not -FArchitecture all --and --not -FMulti-Arch foreign --and --not -FEssential yes \) Packages_$(*F) > Packages_$(*F)_noall_nomaforeign || [ "$$?" -eq 1 ]

Packages_ports_%: Packages_ports_%.gz
	gzip --decompress --stdout Packages_ports_$(*F).gz > Packages_ports_$(*F)

Packages_unreleased_%: Packages_unreleased_%.gz
	gzip --decompress --stdout Packages_unreleased_$(*F).gz > Packages_unreleased_$(*F)

cross_matrix.html: $(MATRIX_HTML) mainarchs.txt portarchs.txt
	python3 buildcheck_matrix.py "$(MAINARCHS)" "$(PORTARCHS)" > cross_matrix.html

##
# Extract from .deb packages
##

debian-ports-archive-keyring.gpg: debian-ports-archive-keyring_2018.12.27_all.deb
	dpkg-deb --fsys-tarfile debian-ports-archive-keyring_2018.12.27_all.deb \
		| tar --extract --strip-components=4 \
			./usr/share/keyrings/debian-ports-archive-keyring.gpg

##
# Some more lists
##

Packages_main_armhf_fake: Packages_main_amd64
	botch-convert-arch amd64 armhf Packages_main_amd64 Packages_main_armhf_fake

Sources_noall: Sources
	grep-dctrl --not -X -F Architecture all Sources > Sources_noall

Sources_noall_armhf: Sources_noall
	botch-add-arch armhf Sources_noall Sources_noall_armhf


##
# cross_all.html
##

# FIXME: allow blacklisting background packages using grep-dctrl, for example:  ^python[23]\.[0-9]+-minimal$
cross_all.yaml: Packages_main_amd64 Packages_main_armhf_fake_noall_nomaforeign crossbuild-essential-amd64-armhf Sources_noall_armhf
	/usr/bin/time -v botch-buildcheck-more-problems --latest 1 --explain --failures \
		--deb-native-arch=amd64 --deb-host-arch=armhf --deb-drop-b-d-indep \
		--progress --verbose --verbose --deb-profiles=cross,nocheck \
		Packages_main_amd64 Packages_main_armhf_fake_noall_nomaforeign crossbuild-essential-amd64-armhf Sources_noall_armhf \
		> cross_all.yaml || [ "$$?" -lt 64 ]

cross_all.html: cross_all.yaml Packages_main_amd64 Packages_main_armhf_fake_noall_nomaforeign crossbuild-essential-amd64-armhf
	$(eval DESCRIPTION=<p>Below are two tables showing the two problem classes that \
		prevent cross compilation on the dependency level. \
		This example tries to satisfy the crossbuild dependencies of \
		all source packages on amd64 as the build architecture for a dummy \
		architecture generated from amd64 (called armhf here) as the host architecture in \
		current Debian sid.</p> \
		<p>A machine parsable version can be retrieved in \
		<a href="cross_all.yaml">dose yaml format</a></p> \
	) \
	/usr/bin/time -v botch-dose2html --wnpp --btsuser debian-cross@lists.debian.org \
		--btstag cross-satisfiability --desc '$(DESCRIPTION)' --timestamp "$(SNAPSHOT)" \
		--srcsdir cross_all --packages Packages_main_amd64 --packages Packages_main_armhf_fake_noall_nomaforeign --packages crossbuild-essential-amd64-armhf --wwwroot=. \
		cross_all.yaml cross_all.html

##
# foreign_install.html
##

foreign_install.yaml: Packages_main_amd64 Packages_main_armhf_fake_noall_nomaforeign crossbuild-essential-amd64-armhf
	/usr/bin/time -v botch-distcheck-more-problems --progress --verbose --verbose \
		--latest 1 --deb-native-arch=amd64 --deb-foreign-archs=armhf --explain \
		--failures --bg deb://Packages_main_amd64 \
		--bg deb://crossbuild-essential-amd64-armhf \
		--fg deb://Packages_main_armhf_fake_noall_nomaforeign > foreign_install.yaml \
		> foreign_install.yaml || [ "$$?" -lt 64 ]

foreign_install.html: foreign_install.yaml Packages_main_amd64 Packages_main_armhf_fake_noall_nomaforeign crossbuild-essential-amd64-armhf
	$(eval DESCRIPTION=<p>Below are two tables showing the two problem classes that \
		prevent installation of foreign architecture binaries</p> \
		<p>We try to install all binary packages from a dummy \
		architecture generated from amd64 (called armhf here) on amd64.</p> \
		<p>A machine parsable version can be retrieved in \
		<a href="foreign_install.yaml">dose yaml format</a></p> \
	) \
	/usr/bin/time -v botch-dose2html --wnpp foreign_install.yaml foreign_install.html \
		--desc '$(DESCRIPTION)' --timestamp "$(SNAPSHOT)" --srcsdir foreign_install \
		--packages Packages_main_amd64 --packages Packages_main_armhf_fake_noall_nomaforeign --packages crossbuild-essential-amd64-armhf --wwwroot=.

##
# co_ma_same.html
##

Packages_co_ma_same: Packages_main_amd64
	grep-dctrl -F Multi-Arch same -s Package -n Packages_main_amd64 | sort | uniq | while read p; do \
	    echo 'Package:' $${p}-pseudo; \
	    echo 'Version: 0.invalid.0'; \
	    echo 'Architecture: amd64'; \
	    echo 'Depends:' $${p}:amd64, $${p}:armhf; \
	    echo; \
	done > Packages_co_ma_same

co_ma_same.yaml: Packages_co_ma_same Packages_main_amd64 Packages_main_armhf_fake_noall_nomaforeign crossbuild-essential-amd64-armhf
	/usr/bin/time -v botch-distcheck-more-problems --progress --verbose --verbose \
		--deb-native-arch=amd64 --deb-foreign-archs=armhf \
		--bg deb://Packages_main_amd64 --bg deb://Packages_main_armhf_fake_noall_nomaforeign \
		--bg deb://crossbuild-essential-amd64-armhf --fg deb://Packages_co_ma_same \
		--explain --failures > co_ma_same.yaml \
		|| [ "$$?" -lt 64 ]

co_ma_same.html: co_ma_same.yaml Packages_main_amd64 Packages_main_armhf_fake_noall_nomaforeign crossbuild-essential-amd64-armhf
	$(eval DESCRIPTION=<p>Below are two tables showing the two problem classes that \
		prevent co-installation of Multi-Arch:same binaries</p> \
		<p>For every M-A:same package we create a dummy package \
		with the -pseudo prefix which depends on that package \
		for amd64 and a dummy architecture generated from amd64 (called armhf here).</p> \
		<p>A machine parsable version can be retrieved in \
		<a href="co_ma_same.yaml">dose yaml format</a></p> \
	) \
	/usr/bin/time -v botch-dose2html --wnpp co_ma_same.yaml co_ma_same.html \
		--desc '$(DESCRIPTION)' --timestamp "$(SNAPSHOT)" --srcsdir co_ma_same \
		--packages Packages_main_amd64 --packages Packages_main_armhf_fake_noall_nomaforeign --packages crossbuild-essential-amd64-armhf --wwwroot=.

##
# ma_interpreter.html
##

ma_interpreter.xml: Packages_main_amd64 Sources
	/usr/bin/time -v dose-ceve --deb-drop-b-d-indep --deb-native-arch=amd64 -G pkg -T grml deb://Packages_main_amd64 debsrc://Sources > ma_interpreter.xml

ma_interpreter.html: ma_interpreter.xml Packages_main_amd64
	/usr/bin/time -v botch-multiarch-interpreter-problem --html --packages Packages_main_amd64 ma_interpreter.xml > ma_interpreter.html

##
# importance_metric.html
##

strongbuildgraph_all.xml: Packages_main_amd64 Sources
	/usr/bin/time -v botch-create-graph --deb-native-arch=amd64 Packages_main_amd64 Sources --strongtype > strongbuildgraph_all.xml

strongsrcgraph_all.xml: strongbuildgraph_all.xml Packages_main_amd64 Sources
	/usr/bin/time -v botch-buildgraph2srcgraph strongbuildgraph_all.xml --deb-native-arch=amd64 Packages_main_amd64 Sources > strongsrcgraph_all.xml

closurebuildgraph_all.xml: Packages_main_amd64 Sources
	/usr/bin/time -v botch-create-graph --deb-native-arch=amd64 Packages_main_amd64 Sources --closuretype > closurebuildgraph_all.xml

closuresrcgraph_all.xml: closurebuildgraph_all.xml Packages_main_amd64 Sources
	/usr/bin/time -v botch-buildgraph2srcgraph closurebuildgraph_all.xml --deb-native-arch=amd64 Packages_main_amd64 Sources > closuresrcgraph_all.xml

importance_metric_all.txt: strongsrcgraph_all.xml closuresrcgraph_all.xml
	/usr/bin/time -v botch-calcportsmetric --online strongsrcgraph_all.xml closuresrcgraph_all.xml > importance_metric_all.txt

strongbuildgraph_noall.xml: Packages_main_amd64 available-all Sources
	/usr/bin/time -v botch-create-graph --available=available-all --deb-native-arch=amd64 Packages_main_amd64 Sources --strongtype > strongbuildgraph_noall.xml

strongsrcgraph_noall.xml: strongbuildgraph_noall.xml Packages_main_amd64 Sources
	/usr/bin/time -v botch-buildgraph2srcgraph strongbuildgraph_noall.xml --deb-native-arch=amd64 Packages_main_amd64 Sources > strongsrcgraph_noall.xml

closurebuildgraph_noall.xml: Packages_main_amd64 available-all Sources
	/usr/bin/time -v botch-create-graph --available=available-all --deb-native-arch=amd64 Packages_main_amd64 Sources --closuretype > closurebuildgraph_noall.xml

closuresrcgraph_noall.xml: closurebuildgraph_noall.xml Packages_main_amd64 Sources
	/usr/bin/time -v botch-buildgraph2srcgraph closurebuildgraph_noall.xml --deb-native-arch=amd64 Packages_main_amd64 Sources > closuresrcgraph_noall.xml

importance_metric_noall.txt: strongsrcgraph_noall.xml closuresrcgraph_noall.xml
	/usr/bin/time -v botch-calcportsmetric --online strongsrcgraph_noall.xml closuresrcgraph_all.xml > importance_metric_noall.txt

##
# essential.html
##

build-essential-amd64: Packages_main_amd64
	grep-dctrl --exact-match --field Package build-essential Packages_main_amd64 | botch-latest-version - - > build-essential-amd64

build-essential-src: build-essential-amd64 Sources
	botch-bin2src --deb-native-arch="amd64" build-essential-amd64 Sources > build-essential-src

available-all: Packages_main_amd64
	grep-dctrl --exact-match -F Architecture all Packages_main_amd64 > available-all

available-all-maforeign: Packages_main_amd64
	grep-dctrl --exact-match \( -F Architecture all --or -F Multi-Arch foreign \) Packages_main_amd64 > available-all-maforeign

minimal: Packages_main_amd64
	grep-dctrl -X \( -FPackage build-essential --or -FPackage apt --or -FPackage debhelper --or -FEssential yes \) Packages_main_amd64 > "minimal"

minimal-amd64: minimal Packages_main_amd64
	dose-deb-coinstall --bg=Packages_main_amd64 --fg=minimal --deb-native-arch=amd64 > minimal-amd64

minimal-amd64-noall: minimal-amd64
	grep-dctrl --not --exact-match -F Architecture all minimal-amd64 > minimal-amd64-noall

minimal-noall-src: Sources minimal-amd64-noall
	botch-bin2src --deb-native-arch=amd64 minimal-amd64-noall Sources > minimal-noall-src

minimal-all-src: Sources minimal-amd64
	botch-bin2src --deb-native-arch=amd64 minimal-amd64 Sources > minimal-all-src

define ESSENTIAL
$1-essential-$2.xml: $3 build-essential-src Sources Packages_main_amd64
	botch-create-graph --deb-native-arch=amd64 $4 --bg Sources Packages_main_amd64 build-essential-src > $1-essential-$2.xml

$1-essential-$2: $1-essential-$2.xml Packages_main_amd64
	botch-buildgraph2packages $1-essential-$2.xml Packages_main_amd64 > $1-essential-$2

$1-essential-$2-src: $1-essential-$2-filtered Sources
	botch-bin2src --deb-native-arch=amd64 $1-essential-$2 Sources > $1-essential-$2-src

endef

GOPT_opt=--optgraph
GOPT_strong=--strongtype
GOPT_closure=--closuretype
AOPT_noall=--available available-all --deb-drop-b-d-indep
AOPT_noall-nomaforeign=--available available-all-maforeign --deb-drop-b-d-indep
ESS_PREREQ_noall=available-all
ESS_PREREQ_noall-nomaforeign=available-all-maforeign

$(eval $(foreach G,opt strong closure, \
	$(foreach A,noall-nomaforeign noall all, \
		$(call ESSENTIAL,$G,$A, \
			$(ESS_PREREQ_$A), \
			$(GOPT_$G) $(AOPT_$A) \
))))

define ESSENTIAL_FILTERED
$1-essential-noall-filtered: $1-essential-noall available-all
	botch-packages-difference $1-essential-noall available-all $1-essential-noall-filtered

$1-essential-noall-nomaforeign-filtered: $1-essential-noall-nomaforeign available-all-maforeign
	botch-packages-difference $1-essential-noall-nomaforeign available-all-maforeign $1-essential-noall-nomaforeign-filtered

$1-essential-all-filtered: $1-essential-all
	ln -sf $1-essential-all $1-essential-all-filtered

endef

$(eval $(foreach G,opt strong closure,$(call ESSENTIAL_FILTERED,$G)))

ESSENTIAL_SRC=$(foreach G,opt strong closure,$(foreach A,noall-nomaforeign noall all,$G-essential-$A-src))
ESSENTIAL_BIN=$(foreach G,opt strong closure,$(foreach A,noall-nomaforeign noall all,$G-essential-$A))
ESSENTIAL_XML=$(foreach G,opt strong closure,$(foreach A,noall-nomaforeign noall all,$G-essential-$A.xml))
ESSENTIAL_BIN_FILTERED=$(foreach G,opt strong closure,$(foreach A,noall-nomaforeign noall all,$G-essential-$A-filtered))

essential.html: $(ESSENTIAL_SRC) minimal-amd64 minimal-noall-src minimal-all-src
	./make_essential.sh > essential.html

##
# history.html
##

# FIXME: this section is not built yet because on more recent snapshot
# timestamps, botch-clean-repository will error with:
#
# Fatal error in module clean-repository.ml:
#  will not remove essential package

-include history.make

HISTORY_CLOSURE_TXT:=$(foreach T,$(TIMESTAMPS),$(T)_closure_history.txt)
HISTORY_DISJ_TXT:=$(foreach T,$(TIMESTAMPS),$(T)_disj_history.txt)
HISTORY_OPTIS_TXT:=$(foreach T,$(TIMESTAMPS),$(T)_optis_history.txt)
HISTORY_OPTUNIV_TXT:=$(foreach T,$(TIMESTAMPS),$(T)_optuniv_history.txt)
HISTORY_CROSS_TXT:=$(foreach T,$(TIMESTAMPS),$(T)_cross_history.txt)

history.make: ./timestamps.py Makefile thisweek
	echo "TIMESTAMPS="$(shell ./timestamps.py) > history.make

define HISTORY_CLEAN

# FIXME: actually check the gpg signature of the release file
$1_release:
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output $1_release http://snapshot.debian.org/archive/debian/$1T000000Z/dists/sid/Release
	TZ=0 touch --date=$1 $1_release

$1_sources_unprocessed.gz: $1_release
	curl --limit-rate 100k --retry-connrefused --fail --retry 4 --output $1_sources_unprocessed.gz http://snapshot.debian.org/archive/debian/$1T000000Z/dists/sid/main/source/Sources.gz
	TZ=0 touch --date=$1 $1_sources_unprocessed.gz
	./checkmd5.sh $1_sources_unprocessed.gz $1_release main/source/Sources.gz || echo | gzip > $1_sources_unprocessed.gz

.INTERMEDIATE: $1_sources_unprocessed
$1_sources_unprocessed: $1_sources_unprocessed.gz
	gzip --decompress --stdout $1_sources_unprocessed.gz > $1_sources_unprocessed

# FIXME: change sanitize-sources.sh such that it outputs on stdout
.INTERMEDIATE: $1_sources_processed
$1_sources_processed: $1_sources_unprocessed
	cp $1_sources_unprocessed $1_sources_processed
	./sanitize-sources.sh $1 $1_sources_processed

$1_i386_packages_unprocessed.gz: $1_release
	curl --limit-rate 100k --retry-connrefused --location --fail --retry 4 --output $1_i386_packages_unprocessed.gz http://snapshot.debian.org/archive/debian/$1T000000Z/dists/sid/main/binary-i386/Packages.gz
	TZ=0 touch --date=$1 $1_i386_packages_unprocessed.gz
	./checkmd5.sh $1_i386_packages_unprocessed.gz $1_release main/binary-i386/Packages.gz || echo | gzip > $1_i386_packages_unprocessed.gz

.INTERMEDIATE: $1_i386_packages_unprocessed
$1_i386_packages_unprocessed: $1_i386_packages_unprocessed.gz
	gzip --decompress --stdout $1_i386_packages_unprocessed.gz > $1_i386_packages_unprocessed

# FIXME: change sanitize-packages.sh such that it outputs on stdout
.INTERMEDIATE: $1_i386_packages
$1_i386_packages: $1_i386_packages_unprocessed
	cp $1_i386_packages_unprocessed $1_i386_packages
	./sanitize-packages.sh $1 $1_i386_packages

.INTERMEDIATE: $1_sources_noall
$1_sources_noall: $1_sources_processed
	grep-dctrl --not --exact-match -F Architecture all $1_sources_processed > $1_sources_noall || true

.INTERMEDIATE: $1_sources
$1_sources: $1_sources_noall
	botch-add-arch mipsel $1_sources_noall $1_sources

.INTERMEDIATE: $1_build_essential
$1_build_essential: $1_i386_packages
	grep-dctrl --exact-match --field Package build-essential $1_i386_packages | botch-latest-version - - > $1_build_essential

.INTERMEDIATE: $1_build_essential_src
$1_build_essential_src: $1_build_essential $1_sources
	botch-bin2src --deb-native-arch=i386 $1_build_essential $1_sources > $1_build_essential_src

.INTERMEDIATE: $1_i386_available_all
$1_i386_available_all: $1_i386_packages
	grep-dctrl --exact-match -F Architecture all $1_i386_packages > $1_i386_available_all || true

.INTERMEDIATE: $1_closuregraph.xml
$1_closuregraph.xml: $1_i386_available_all $1_sources $1_i386_packages $1_build_essential_src
	botch-create-graph --deb-drop-b-d-indep --deb-native-arch=i386 \
		--available $1_i386_available_all --closuretype \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		--bg $1_sources $1_i386_packages $1_build_essential_src \
		> $1_closuregraph.xml

.INTERMEDIATE: $1_closure_i386_packages
$1_closure_i386_packages: $1_closuregraph.xml
	botch-buildgraph2packages $1_closuregraph.xml $1_i386_packages > $1_closure_i386_packages

.INTERMEDIATE: $1_closure_i386_packages_noall
$1_closure_i386_packages_noall: $1_closure_i386_packages
	grep-dctrl --not --exact-match -F Architecture all $1_closure_i386_packages > $1_closure_i386_packages_noall || true

.INTERMEDIATE: $1_closure_sources
$1_closure_sources: $1_closure_i386_packages_noall $1_sources
	botch-bin2src \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		--deb-native-arch=i386 $1_closure_i386_packages_noall $1_sources \
		> $1_closure_sources

.INTERMEDIATE: $1_closure_i386_packages_clean
$1_closure_i386_packages_clean: $1_closure_i386_packages $1_closure_sources
	botch-clean-repository --deb-drop-b-d-indep --deb-native-arch=i386 \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		$1_closure_i386_packages $1_closure_sources \
		> $1_closure_i386_packages_clean || true

.INTERMEDIATE: $1_closure_i386_packages_clean_noall
$1_closure_i386_packages_clean_noall: $1_closure_i386_packages_clean
	grep-dctrl --not --exact-match -F Architecture all $1_closure_i386_packages_clean > $1_closure_i386_packages_clean_noall || true

.INTERMEDIATE: $1_closure_sources_clean
$1_closure_sources_clean: $1_closure_i386_packages_clean_noall $1_closure_sources
	botch-bin2src \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		--deb-native-arch=i386 $1_closure_i386_packages_clean_noall $1_closure_sources \
		> $1_closure_sources_clean
	dose-builddebcheck --deb-native-arch=i386 --deb-drop-b-d-indep $1_closure_i386_packages_clean $1_closure_sources_clean

.INTERMEDIATE: $1_closure_mipsel_packages_clean
$1_closure_mipsel_packages_clean: $1_closure_i386_packages_clean
	botch-convert-arch i386 mipsel $1_closure_i386_packages_clean - > $1_closure_mipsel_packages_clean

.INTERMEDIATE: $1_closure_mipsel_packages_clean_noall_nomaforeign
$1_closure_mipsel_packages_clean_noall_nomaforeign: $1_closure_mipsel_packages_clean
	grep-dctrl -X \( --not -FArchitecture all --and --not -FMulti-Arch foreign --and --not -FEssential yes \) $1_closure_mipsel_packages_clean > $1_closure_mipsel_packages_clean_noall_nomaforeign || true

.INTERMEDIATE: $1_disj_i386_packages
$1_disj_i386_packages: $1_closure_i386_packages_clean
	botch-remove-virtual-disjunctions $1_closure_i386_packages_clean /dev/null $1_disj_i386_packages /dev/null

.INTERMEDIATE: $1_disj_sources
$1_disj_sources: $1_closure_sources_clean
	botch-remove-virtual-disjunctions /dev/null $1_closure_sources_clean /dev/null $1_disj_sources

.INTERMEDIATE: $1_disj_i386_packages_clean
$1_disj_i386_packages_clean: $1_disj_i386_packages $1_disj_sources
	botch-clean-repository --deb-drop-b-d-indep --deb-native-arch=i386 \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		$1_disj_i386_packages $1_disj_sources \
		> $1_disj_i386_packages_clean || true

.INTERMEDIATE: $1_disj_i386_packages_clean_noall
$1_disj_i386_packages_clean_noall: $1_disj_i386_packages_clean
	grep-dctrl --not --exact-match -F Architecture all $1_disj_i386_packages_clean > $1_disj_i386_packages_clean_noall || true

.INTERMEDIATE: $1_disj_sources_clean
$1_disj_sources_clean: $1_disj_i386_packages_clean_noall $1_disj_sources
	botch-bin2src \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		--deb-native-arch=i386 $1_disj_i386_packages_clean_noall $1_disj_sources \
		> $1_disj_sources_clean
	dose-builddebcheck --deb-native-arch=i386 --deb-drop-b-d-indep $1_disj_i386_packages_clean $1_disj_sources_clean

.INTERMEDIATE: $1_optuniv_i386_packages
$1_optuniv_i386_packages: $1_closure_i386_packages_clean $1_closure_sources_clean
	botch-optuniv --deb-native-arch=i386 --deb-drop-b-d-indep \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		$1_closure_i386_packages_clean $1_closure_sources_clean \
		> $1_optuniv_i386_packages

.INTERMEDIATE: $1_optuniv_i386_packages_noall
$1_optuniv_i386_packages_noall: $1_optuniv_i386_packages
	grep-dctrl --not --exact-match -F Architecture all $1_optuniv_i386_packages > $1_optuniv_i386_packages_noall || true

.INTERMEDIATE: $1_optuniv_sources
$1_optuniv_sources: $1_optuniv_i386_packages_noall $1_closure_sources_clean
	botch-bin2src --deb-native-arch=i386 \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		$1_optuniv_i386_packages_noall $1_closure_sources_clean \
		> $1_optuniv_sources

.INTERMEDIATE: $1_optuniv_i386_packages_clean
$1_optuniv_i386_packages_clean: $1_optuniv_i386_packages $1_optuniv_sources
	botch-clean-repository --deb-drop-b-d-indep --deb-native-arch=i386 \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		$1_optuniv_i386_packages $1_optuniv_sources \
		> $1_optuniv_i386_packages_clean || true

.INTERMEDIATE: $1_optuniv_i386_packages_clean_noall
$1_optuniv_i386_packages_clean_noall: $1_optuniv_i386_packages_clean
	grep-dctrl --not --exact-match -F Architecture all $1_optuniv_i386_packages_clean > $1_optuniv_i386_packages_clean_noall || true

.INTERMEDIATE: $1_optuniv_sources_clean
$1_optuniv_sources_clean: $1_optuniv_i386_packages_clean_noall $1_optuniv_sources
	botch-bin2src \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		--deb-native-arch=i386 $1_optuniv_i386_packages_clean_noall $1_optuniv_sources \
		> $1_optuniv_sources_clean
	dose-builddebcheck --deb-native-arch=i386 --deb-drop-b-d-indep $1_optuniv_i386_packages_clean $1_optuniv_sources_clean

.INTERMEDIATE: $1_optis_i386_packages_clean
$1_optis_i386_packages_clean: $1_closure_i386_packages_clean
	ln -sf $1_closure_i386_packages_clean $1_optis_i386_packages_clean

.INTERMEDIATE: $1_optis_sources_clean
$1_optis_sources_clean: $1_closure_sources_clean
	ln -sf $1_closure_sources_clean $1_optis_sources_clean

endef

define HISTORY
.INTERMEDIATE: $1_$2_minimal
$1_$2_minimal: $1_$2_i386_packages_clean
	grep-dctrl -X \( \
		-FPackage build-essential --or \
		-FPackage apt --or \
		-FPackage debhelper --or \
		-FEssential yes \
		\) $1_$2_i386_packages_clean \
		> $1_$2_minimal || true

.INTERMEDIATE: $1_$2_minimal_coinst
$1_$2_minimal_coinst: $1_$2_i386_packages_clean $1_$2_minimal
	dose-deb-coinstall --bg="$1_$2_i386_packages_clean" --fg=$1_$2_minimal \
		--deb-native-arch=i386 > $1_$2_minimal_coinst || true

.INTERMEDIATE: $1_$2_minimal_coinst_noall
$1_$2_minimal_coinst_noall: $1_$2_minimal_coinst
	grep-dctrl --not --exact-match -F Architecture all $1_$2_minimal_coinst > $1_$2_minimal_coinst_noall || true

.INTERMEDIATE: $1_$2_crossed_src
$1_$2_crossed_src: $1_$2_minimal_coinst_noall $1_$2_sources_clean
	botch-bin2src --deb-native-arch=i386 \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		$1_$2_minimal_coinst_noall $1_$2_sources_clean > $1_$2_crossed_src

.INTERMEDIATE: $1_$2_available_all
$1_$2_available_all: $1_$2_i386_packages_clean
	grep-dctrl -FArchitecture all $1_$2_i386_packages_clean > $1_$2_available_all || true

.INTERMEDIATE: $1_$2_available_crossed
$1_$2_available_crossed: $1_$2_sources_clean $1_$2_available_all $1_$2_i386_packages_clean $1_$2_crossed_src
	botch-src2bin --deb-native-arch=i386 \
		$(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		--bg "$1_$2_sources_clean" -A $1_$2_available_all \
		"$1_$2_i386_packages_clean" $1_$2_crossed_src > $1_$2_available_crossed

.INTERMEDIATE: $1_$2_available
$1_$2_available: $1_$2_available_crossed $1_$2_available_all
	botch-packages-union $1_$2_available_crossed $1_$2_available_all - > $1_$2_available

.INTERMEDIATE: $1_$2_buildgraph.xml
$1_$2_buildgraph.xml: $1_$2_i386_packages_clean $1_$2_sources_clean $1_$2_available
	botch-create-graph -v --deb-native-arch=i386 --deb-ignore-essential \
		$(shell [ $2 = optis ] && echo --optgraph ) \
		-A $1_$2_available $(shell [ $1 -le 20091201 ] && echo --allowsrcmismatch) \
		--deb-drop-b-d-indep "$1_$2_i386_packages_clean" "$1_$2_sources_clean" \
		> "$1_$2_buildgraph.xml"

# FIXME: add an option to botch-extract-scc that only extract the biggest scc to stdout
$1_$2_history.txt: $1_$2_buildgraph.xml
	tempdir=$$$$(mktemp --directory --suffix=.botch); \
	(numverts=$$$$(botch-extract-scc --outdir="$$$$tempdir" "$1_$2_buildgraph.xml" cyclic | while read name; do \
		botch-graph-info "$$$$tempdir/$$$$name" | cut --fields=2,3; \
		rm "$$$$tempdir/$$$$name"; \
		done | sort --reverse --numeric-sort --key=2 | head -1 ); \
		if [ -n "$$$$numverts" ]; then \
			printf "$1\t$$$$numverts\n"; \
		else \
			printf "# $1\tFAILED\n"; \
		fi) > $1_$2_history.txt; \
	rmdir "$$$$tempdir"

endef

define HISTORY_CROSS
# FIXME: a dose bug outputs the wrong number of source packages (that's why we
# compare with "1 0" and not with "0 0"), see
# https://gforge.inria.fr/tracker/index.php?func=detail&aid=21165&group_id=4395&atid=13808
$1_cross_history.txt: $1_closure_mipsel_packages_clean_noall_nomaforeign $1_closure_i386_packages_clean $1_closure_sources_clean crossbuild-essential-i386-mipsel
	(numsrcs=$$$$(dose-builddebcheck --deb-native-arch=i386 --deb-host-arch=mipsel \
		$1_closure_i386_packages_clean \
		$1_closure_mipsel_packages_clean_noall_nomaforeign \
		crossbuild-essential-i386-mipsel \
		$1_closure_sources_clean \
		| python3 -c 'import sys; from yaml import load, CBaseLoader; d=load(sys.stdin, Loader=CBaseLoader);print(d["source-packages"], d["broken-packages"])'); \
		if [ "$$$$numsrcs" != "1 0" ]; then \
			echo "$1 $$$$numsrcs"; \
		else \
			echo "# $1 FAILED"; \
		fi) > $1_cross_history.txt

endef

$(eval $(foreach T,$(TIMESTAMPS),$(call HISTORY_CLEAN,$T)))

$(eval $(foreach T,$(TIMESTAMPS),$(foreach t,closure disj optis optuniv,$(call HISTORY,$T,$t))))

$(eval $(foreach T,$(TIMESTAMPS),$(call HISTORY_CROSS,$T)))

history_closure.txt: $(HISTORY_CLOSURE_TXT) history.make
	cat $(HISTORY_CLOSURE_TXT) > history_closure.txt

history_disj.txt: $(HISTORY_DISJ_TXT) history.make
	cat $(HISTORY_DISJ_TXT) > history_disj.txt

history_optis.txt: $(HISTORY_OPTIS_TXT) history.make
	cat $(HISTORY_OPTIS_TXT) > history_optis.txt

history_optuniv.txt: $(HISTORY_OPTUNIV_TXT) history.make
	cat $(HISTORY_OPTUNIV_TXT) > history_optuniv.txt

history_cross.txt: $(HISTORY_CROSS_TXT) history.make
	cat $(HISTORY_CROSS_TXT) > history_cross.txt

history.svg: history_closure.txt history_optis.txt history_optuniv.txt history_disj.txt history.gpl
	gnuplot history.gpl

history_cross.svg: history_cross.txt history_cross.gpl
	gnuplot history_cross.gpl

##
# botch-native
##

botch-native/amd64/packages-clean: Packages_main_amd64 Sources
	ret=0; \
	botch-clean-repository --verbose --deb-drop-b-d-indep --deb-native-arch=amd64 Packages_main_amd64 Sources > $@ || ret=$$?; \
	if [ $$ret -ne 0 ]; then \
		botch-y-u-no-bootstrap --verbose --output=botch-native/amd64 amd64 Packages_main_amd64 Sources; \
		touch $@; \
	fi

.INTERMEDIATE: botch-native/amd64/packages-clean-noall
botch-native/amd64/packages-clean-noall: botch-native/amd64/packages-clean
	# might be empty if botch-clean-repository failed
	if [ -s "$<" ]; then \
		grep-dctrl --not --exact-match -F Architecture all $< > $@; \
	else \
		touch $@; \
	fi

.INTERMEDIATE: botch-native/amd64/packages-clean-all
botch-native/amd64/packages-clean-all: botch-native/amd64/packages-clean
	# might be empty if botch-clean-repository failed
	if [ -s "$<" ]; then \
		grep-dctrl --exact-match -F Architecture all $< > $@; \
	else \
		touch $@; \
	fi

.INTERMEDIATE: botch-native/amd64/minimal-select
botch-native/amd64/minimal-select: botch-native/amd64/packages-clean
	# might be empty if botch-clean-repository failed
	if [ -s "$<" ]; then \
		grep-dctrl -X \( -FPackage build-essential --or -FPackage apt --or -FPackage debhelper --or -FEssential yes \) $< > $@; \
	else \
		touch $@; \
	fi

.INTERMEDIATE: botch-native/amd64/minimal-coinst
botch-native/amd64/minimal-coinst: botch-native/amd64/packages-clean botch-native/amd64/minimal-select
	# might be empty if botch-clean-repository failed
	if [ -s "$<" ]; then \
		dose-deb-coinstall --bg=botch-native/amd64/packages-clean --fg=botch-native/amd64/minimal-select --deb-native-arch=amd64 > $@; \
	else \
		touch $@; \
	fi

.INTERMEDIATE: botch-native/amd64/cross-bootstrappable-src
botch-native/amd64/cross-bootstrappable-src: botch-native/cross-bootstrappable Sources
	grep-dctrl -P -e '^('$$(tr '\n' '|' < botch-native/cross-bootstrappable)')$$' Sources > $@

.INTERMEDIATE: botch-native/amd64/cross-bootstrappable
botch-native/amd64/cross-bootstrappable: botch-native/amd64/cross-bootstrappable-src Sources botch-native/amd64/packages-clean
	botch-src2bin --verbose --deb-native-arch=amd64 --bg Sources botch-native/amd64/packages-clean botch-native/amd64/cross-bootstrappable-src > $@

.INTERMEDIATE: botch-native/amd64/available
botch-native/amd64/available: botch-native/amd64/minimal-coinst botch-native/amd64/packages-clean-all botch-native/amd64/cross-bootstrappable
	botch-packages-union --verbose botch-native/amd64/minimal-coinst botch-native/amd64/packages-clean-all botch-native/amd64/cross-bootstrappable $@

.INTERMEDIATE: botch-native/amd64/not-available
botch-native/amd64/not-available: botch-native/amd64/packages-clean botch-native/amd64/available
	botch-packages-difference botch-native/amd64/packages-clean botch-native/amd64/available $@

.INTERMEDIATE: botch-native/amd64/sources-clean
botch-native/amd64/sources-clean: botch-native/amd64/not-available Sources
	botch-bin2src --verbose --deb-native-arch=amd64 botch-native/amd64/not-available Sources > $@

botch-native/amd64/sources-clean-nocheck: botch-native/amd64/sources-clean
	./reduced_profiles.pl - < $< > $@

.INTERMEDIATE: botch-native/amd64/tocompile
botch-native/amd64/tocompile: botch-native/amd64/sources-clean-nocheck botch-native/amd64/not-available
	botch-bin2src --deb-native-arch=amd64 botch-native/amd64/not-available botch-native/amd64/sources-clean-nocheck > botch-native/amd64/tocompile

botch-native/amd64/buildgraph.xml: botch-native/amd64/packages-clean botch-native/amd64/available botch-native/amd64/sources-clean-nocheck botch-native/amd64/tocompile
	# might be empty if botch-clean-repository failed
	if [ -s "$<" ]; then \
		botch-create-graph --verbose --deb-drop-b-d-indep --optgraph --jobs=1 --deb-ignore-essential -A botch-native/amd64/available --deb-native-arch=amd64 --bg botch-native/amd64/sources-clean-nocheck botch-native/amd64/packages-clean botch-native/amd64/tocompile > $@; \
	else \
		touch $@; \
	fi

botch-native/amd64/srcgraph.xml: botch-native/amd64/packages-clean botch-native/amd64/buildgraph.xml botch-native/amd64/sources-clean-nocheck botch-native/amd64/tocompile
	# might be empty if botch-clean-repository failed
	if [ -s "$<" ]; then \
		botch-buildgraph2srcgraph --verbose --deb-drop-b-d-indep botch-native/amd64/buildgraph.xml --deb-native-arch=amd64 --bg botch-native/amd64/sources-clean-nocheck botch-native/amd64/packages-clean botch-native/amd64/tocompile > $@; \
		botch-annotate-strong --verbose --deb-drop-b-d-indep botch-native/amd64/buildgraph.xml $@ --deb-native-arch=amd64 --bg botch-native/amd64/sources-clean-nocheck botch-native/amd64/packages-clean botch-native/amd64/tocompile; \
	else \
		touch $@; \
	fi

botch-native/amd64/stats.json: botch-native/amd64/packages-clean botch-native/amd64/sources-clean-nocheck botch-native/amd64/srcgraph.xml botch-native/amd64/buildgraph.xml botch-native/amd64/available botch-native/amd64/tocompile botch-native/droppable.list
	# might be empty if botch-clean-repository failed
	if [ -s "$<" ]; then \
		botch-print-stats --verbose --deb-drop-b-d-indep --max-length=4 --max-length-fas=6 --sapsb --remove-reduced=botch-native/droppable.list -A botch-native/amd64/available botch-native/amd64/buildgraph.xml botch-native/amd64/srcgraph.xml --deb-native-arch=amd64 --bg botch-native/amd64/sources-clean-nocheck botch-native/amd64/packages-clean botch-native/amd64/tocompile | ydump > $@; \
	else \
		touch $@; \
	fi

botch-native/amd64/stats.html: botch-native/amd64/stats.json
	# botch-native --verbose --debug --output=./botch-native/amd64.tmp --deb-drop-b-d-indep --online --allowsrcmismatch --ignoresrclessbin --no-drop --clean --strong --sapsb --no-fixpoint --optgraph amd64 Packages_main_amd64 Sources 2>&1 && echo "success" || echo "failed"; } | tee ./botch-native/amd64.tmp/log.txt;
	botch-stat-html --verbose --online $< > $@


##
# clean
##

.PHONY: clean
clean:
	rm -f $(TARGETS)
	rm -f matrix.make
	rm -f $(PACKAGES_MAIN_UNPACKED) $(PACKAGES_MAIN_NOALL_NOMAFOREIGN)
	rm -f $(PACKAGES_PORTS_UNPACKED) $(PACKAGES_PORTS_NOALL_NOMAFOREIGN)
	rm -f $(PACKAGES_UNRELEASED_UNPACKED) $(PACKAGES_UNRELEASED_NOALL_NOMAFOREIGN)
	rm -f $(MATRIX_HTML) $(MATRIX_YAML) $(MATRIX_PREREQ)
	rm -f mainarchs.txt mainarchs.make
	rm -f portarchs.txt portarchs.make
	rm -f debian-ports-archive-keyring.gpg
	rm -f thisweek
	rm -f Packages_main_armhf_fake Packages_co_ma_same
	rm -f Sources Sources_noall Sources_noall_armhf
	rm -f Packages_main_armhf_fake_noall_nomaforeign
	rm -f co_ma_same.yaml cross_all.yaml foreign_install.yaml ma_interpreter.xml
	rm -f cross_all/*.html co_ma_same/*.html foreign_install/*.html
	rm -fd cross_all co_ma_same foreign_install
	rm -f bin/botch-add-arch bin/botch-buildcheck-more-problems bin/botch-buildgraph2srcgraph bin/botch-calcportsmetric bin/botch-convert-arch bin/botch-create-graph bin/botch-distcheck-more-problems bin/botch-dose2html bin/botch-multiarch-interpreter-problem bin/botch-bin2src bin/botch-buildgraph2packages bin/botch-latest-version bin/botch-packages-difference bin/botch-clean-repository bin/botch-extract-scc bin/botch-graph-info bin/botch-optuniv bin/botch-packages-union bin/botch-remove-virtual-disjunctions bin/botch-src2bin
	rm -fd py/debarch.py py/__pycache__/util.*.pyc py/util.py
	rm -fd py/__pycache__ py bin
	rm -f build-essential-amd64 build-essential-src available-all available-all-maforeign minimal minimal-amd64 minimal-noall-src minimal-all-src
	rm -f $(ESSENTIAL_SRC) $(ESSENTIAL_BIN) $(ESSENTIAL_XML) $(ESSENTIAL_BIN_FILTERED)
	rm -f closure-essential-*.list opt-essential-*.list strong-essential-*.list # FIXME let make_essential.sh not create these files
	rm -f history.make
	rm -f history.svg history_closure.txt history_disj.txt history_optuniv.txt history_optis.txt

.PHONY: distclean
distclean: clean
	rm -f Sources.xz
	rm -f $(PACKAGES_MAIN) $(PACKAGES_PORTS) $(PACKAGES_UNRELEASED)
	rm -f release release.gpg
	rm -f ports-release ports-release.gpg
	rm -f debian-ports-archive-keyring_2018.12.27_all.deb
