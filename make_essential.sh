#!/bin/sh

packages2plainlist() {
	set +x
	grep-dctrl --no-field-names --show-field=Package,Version,Architecture '' \
		| paste --serial --delimiter="   \n" \
		| while read p v a; do
			echo "$p:$a (= $v)";
		done
	set -x
}

sources2plainlist() {
	set +x
	grep-dctrl --no-field-names --show-field=Package,Version '' \
		| paste --serial --delimiter="  \n" \
		| while read p v; do
			echo "src:$p (= $v)";
		done
	set -x
}


set -exu

arch=amd64
packages=../packages_$arch.gz
sources=../sources.gz

packages2plainlist < "minimal-$arch" | sort > "minimal-$arch.list"

sources2plainlist < "minimal-noall-src" | sort > "minimal-noall-src.list"

sources2plainlist < "minimal-all-src" | sort > "minimal-all-src.list"

make_list() {
	gtype=$1
	avail=$2
	case $gtype in
		opt)     gopt=--optgraph;;
		strong)  gopt=--strongtype;;
		closure) gopt=--closuretype;;
		*)       exit 1;;
	esac
	case $avail in
		noall-nomaforeign | noall)
			gopt="$gopt --available available-$avail --drop-b-d-indep";;
		all)	gopt="$gopt";;
		*)	exit 1;;
	esac
	packages2plainlist < $gtype-essential-$avail | sort > "$gtype-essential-$avail.list"
	sources2plainlist < $gtype-essential-$avail-src | sort > "$gtype-essential-$avail-src.list"
}

for gtype in opt strong closure; do
	for avail in noall-nomaforeign noall all; do
		make_list $gtype $avail
	done
done

# sanity checks
is_subset() {
	if [ "`comm -23 \"$1\" \"$2\" | wc -l`" -ne 0 ]; then
		echo $1 is not a subset of $2
		exit 1
	fi
}

is_subset minimal-noall-src.list minimal-all-src.list

for avail in noall-nomaforeign noall all; do
	for suffix in "" "-src"; do
		is_subset strong-essential-$avail$suffix.list opt-essential-$avail$suffix.list
		is_subset opt-essential-$avail$suffix.list closure-essential-$avail$suffix.list
	done
done

packages2html() {
	set +x
	while read pkg; do
		name=${pkg%%:*}
		echo "<a title=\"$pkg\">$name</a> "
	done
	set -x
}

sources2html() {
	set +x
	while read pkg; do
		name=${pkg#src:}
		name=${name%% *}
		echo "<a title=\"$pkg\">$name</a> "
	done
	set -x
}

coinst=`packages2html < minimal-$arch.list`
minimal_noall_src=`sources2html < minimal-noall-src.list`
minimal_all_src=`comm -13 minimal-noall-src.list minimal-all-src.list | sources2html`
strong_ess_all=`packages2html < strong-essential-all.list`
strong_ess_all_src=`sources2html < strong-essential-all-src.list`
strong_ess_noall=`packages2html < strong-essential-noall.list`
strong_ess_noall_src=`sources2html < strong-essential-noall-src.list`
strong_ess_noall_nomaforeign=`packages2html < strong-essential-noall-nomaforeign.list`
strong_ess_noall_nomaforeign_src=`sources2html < strong-essential-noall-nomaforeign-src.list`
strong_ess_all_num=`wc -l < strong-essential-all.list`
strong_ess_all_src_num=`wc -l < strong-essential-all-src.list`
strong_ess_noall_num=`wc -l < strong-essential-noall.list`
strong_ess_noall_src_num=`wc -l < strong-essential-noall-src.list`
strong_ess_noall_nomaforeign_num=`wc -l < strong-essential-noall-nomaforeign.list`
strong_ess_noall_nomaforeign_src_num=`wc -l < strong-essential-noall-nomaforeign-src.list`
opt_ess_all=`comm -23 opt-essential-all.list strong-essential-all.list | packages2html`
opt_ess_all_src=`comm -23 opt-essential-all-src.list strong-essential-all-src.list | sources2html`
opt_ess_noall=`comm -23 opt-essential-noall.list strong-essential-noall.list | packages2html`
opt_ess_noall_src=`comm -23 opt-essential-noall-src.list strong-essential-noall-src.list | sources2html`
opt_ess_noall_nomaforeign=`comm -23 opt-essential-noall-nomaforeign.list strong-essential-noall-nomaforeign.list | packages2html`
opt_ess_noall_nomaforeign_src=`comm -23 opt-essential-noall-nomaforeign-src.list strong-essential-noall-nomaforeign-src.list | sources2html`
opt_ess_all_num=`wc -l < opt-essential-all.list`
opt_ess_all_src_num=`wc -l < opt-essential-all-src.list`
opt_ess_noall_num=`wc -l < opt-essential-noall.list`
opt_ess_noall_src_num=`wc -l < opt-essential-noall-src.list`
opt_ess_noall_nomaforeign_num=`wc -l < opt-essential-noall-nomaforeign.list`
opt_ess_noall_nomaforeign_src_num=`wc -l < opt-essential-noall-nomaforeign-src.list`
closure_ess_all=`comm -23 closure-essential-all.list opt-essential-all.list | packages2html`
closure_ess_all_src=`comm -23 closure-essential-all-src.list opt-essential-all-src.list | sources2html`
closure_ess_noall=`comm -23 closure-essential-noall.list opt-essential-noall.list | packages2html`
closure_ess_noall_src=`comm -23 closure-essential-noall-src.list opt-essential-noall-src.list | sources2html`
closure_ess_noall_nomaforeign=`comm -23 closure-essential-noall-nomaforeign.list opt-essential-noall-nomaforeign.list | packages2html`
closure_ess_noall_nomaforeign_src=`comm -23 closure-essential-noall-nomaforeign-src.list opt-essential-noall-nomaforeign-src.list | sources2html`
closure_ess_all_num=`wc -l < closure-essential-all.list`
closure_ess_all_src_num=`wc -l < closure-essential-all-src.list`
closure_ess_noall_num=`wc -l < closure-essential-noall.list`
closure_ess_noall_src_num=`wc -l < closure-essential-noall-src.list`
closure_ess_noall_nomaforeign_num=`wc -l < closure-essential-noall-nomaforeign.list`
closure_ess_noall_nomaforeign_src_num=`wc -l < closure-essential-noall-nomaforeign-src.list`

cat << END
<html>
<body>
  <h1>Table of contents</h1>
  <ol>
    <li><a href="#mbs">Minimal build system</a>
      <ol>
        <li>Binary packages</li>
        <li>Source packages (excluding those building Arch:all)</li>
        <li>Source packages (only those building Arch:all)</li>
      </ol>
    </li>
    <li><a href="#bdtransitiveess">B-D-transitive essential</a>
      <ol>
        <li>Summary</li>
        <li>Re-use existing Architecture:all and M-A:foreign</li>
        <li>Re-use existing Architecture:all</li>
         <li>Bootstrap Architecture:all as well</li>
      </ol>
    </li>
    <li><a href="#downloads">Downloadable lists</a>
      <ol>
        <li>Essential coinstallation set</li>
        <li>Re-use existing Architecture:all and M-A:foreign</li>
        <li>Re-use existing Architecture:all</li>
        <li>Bootstrap Architecture:all as well</li>
      </ol>
    </li>
  </ol>
  <h1><a name="mbs" />Minimal build system</h1>
  <p>A coinstallation set of all Essential:yes packages, apt, debhelper and build-essential for $arch.</p>
  <h2>Binary packages</h2>
  $coinst
  <h2>Source packages (excluding those building Arch:all)</h2>
  $minimal_noall_src
  <h2>Source packages (only those building Arch:all)</h2>
  $minimal_all_src
  <h1><a name="bdtransitiveess" />B-D-transitive essential</h1>

  <p>All source, respective binary packages that are transitively
  build-essential (or essential - this is equivalent) under the Depends and
  Build-Depends relationships using three different dependency traversal
  methods. The three methods are called <b>Strong</b>, <b>Opt. IS</b> and
  <b>Closure</b> and are explained in the following three paragraphs,
  respectively. The used terminology is explained in the <a
  href="https://gitlab.mister-muffin.de/debian-bootstrap/botch/wikis/Terminology">wiki</a>.<p>

  <ul>
  <li><b>Strong</b>: Starting from src:build-essential, find all strong
  dependencies, retrieve the source packages that build these binary packages
  and recurse until no more vertices are added to the graph.</li>

  <li><b>Opt. IS</b>: The name is short for "Optimal Installation Set". Starting
  from src:build-essential, compute an installation set with a minimal number
  of binary packages, retrieve the source packages that build these binary
  packages and recurse until no more vertices are added to the graph. This set
  is a superset of <b>Strong</b> and thus only the additional packages are
  shown in the full table. The summary table lists the total number of packages
  in each set.</li>

  <li><b>Closure</b>: Starting from src:build-essential, traverse every
  dependency relationship, retrieve the source packages that build these binary
  packages and recurse until no more vertices are added to the graph. This set
  is a superset of <b>Opt. IS</b> and thus only the additional packages are
  shown in the full table. The summary table lists the total number of packages
  in each set.</li>
  </ul>

  <p>These three traversal methods are each applied to three scenarios. The
  three scenarios are <b>re-use existing Architecture:all and M-A:foreign</b>,
  <b>re-use existing Architecture:all</b> and <b>Bootstrap Architecture:all as
  well</b></p>

  <ul>
  <li><b>Re-use existing Architecture:all and M-A:foreign</b>: In this method the
  dependency graph will not connect binary packages that are either
  Architecture:all or Multi-Arch:foreign to the source packages they build
  from. This set is calculated because missing metadata makes it impossible to
  create a dependency graph for the cross bootstrap phase. As a workaround, a
  native bootstrap is assumed but, just as during the cross phase,
  Multi-Arch:foreign binaries are assumed to already exist so that their source
  packages do not need to be compiled to create them.</li>

  <li><b>Re-use existing Architecture:all</b>: In this method the dependency graph
  will not connect binary packages that are Architecture:all to the source
  packages they build from. This is the situation in a regular native bootstrap
  of Debian for a new architecture, as existing Architecture:all packages that
  were already built and are in the archive can just be re-used to satisfy
  dependencies during a native bootstrap.</li>

  <li><b>Bootstrap Architecture:all as well</b>: In this method the dependency
  graph will connect all binary packages to their respective source packages.
  This represents a bootstrap where even Architecture:all packages are being
  bootstrapped. This is useful if one wants to ensure that no dependencies come
  from any other source than the bootstrapped repository, for example for
  security, trustring trust and paranoia reasons.</li>
  </ul>

  <input type="button" onclick="var d=document.getElementsByClassName('binary');for(var i=0;i<d.length;i++){if(d[i].style.display!=='none'){d[i].style.display='none'}else{d[i].style.display='block'}}" value="Hide/Show binary columns" />

  <h2>Summary</h2>
  <h3>Re-use existing Architecture:all and M-A:foreign</h3>
  <table border="1">
    <tr><td>&nbsp;</td><td>Source packages</td><td class="binary" style="display:none">Binary packages</td></tr>
    <tr><td>Strong</td><td>$strong_ess_noall_nomaforeign_src_num</td><td class="binary" style="display:none">$strong_ess_noall_nomaforeign_num</td></tr>
    <tr><td>Opt. IS</td><td>$opt_ess_noall_nomaforeign_src_num</td><td class="binary" style="display:none">$opt_ess_noall_nomaforeign_num</td></tr>
    <tr><td>Closure</td><td>$closure_ess_noall_nomaforeign_src_num</td><td class="binary" style="display:none">$closure_ess_noall_nomaforeign_num</td></tr>
  </table>
  <h3>Re-use existing Architecture:all</h3>
  <table border="1">
    <tr><td>&nbsp;</td><td>Source packages</td><td class="binary" style="display:none">Binary packages</td></tr>
    <tr><td>Strong</td><td>$strong_ess_noall_src_num</td><td class="binary" style="display:none">$strong_ess_noall_num</td></tr>
    <tr><td>Opt. IS</td><td>$opt_ess_noall_src_num</td><td class="binary" style="display:none">$opt_ess_noall_num</td></tr>
    <tr><td>Closure</td><td>$closure_ess_noall_src_num</td><td class="binary" style="display:none">$closure_ess_noall_num</td></tr>
  </table>
  <h3>Bootstrap Architecture:all as well</h3>
  <table border="1">
    <tr><td>&nbsp;</td><td>Source packages</td><td class="binary" style="display:none">Binary packages</td></tr>
    <tr><td>Strong</td><td>$strong_ess_all_src_num</td><td class="binary" style="display:none">$strong_ess_all_num</td></tr>
    <tr><td>Opt. IS</td><td>$opt_ess_all_src_num</td><td class="binary" style="display:none">$opt_ess_all_num</td></tr>
    <tr><td>Closure</td><td>$closure_ess_all_src_num</td><td class="binary" style="display:none">$closure_ess_all_num</td></tr>
  </table>

  <h2>Re-use existing Architecture:all and M-A:foreign</h2>
  <table border="1">
    <tr><td>&nbsp;</td><td>Source packages</td><td class="binary" style="display:none">Binary packages</td></tr>
    <tr><td>Strong</td><td>$strong_ess_noall_nomaforeign_src</td><td class="binary" style="display:none">$strong_ess_noall_nomaforeign</td></tr>
    <tr><td>Opt. IS</td><td>$opt_ess_noall_nomaforeign_src</td><td class="binary" style="display:none">$opt_ess_noall_nomaforeign</td></tr>
    <tr><td>Closure</td><td>$closure_ess_noall_nomaforeign_src</td><td class="binary" style="display:none">$closure_ess_noall_nomaforeign</td></tr>
  </table>
  <h2>Re-use existing Architecture:all</h2>
  <table border="1">
    <tr><td>&nbsp;</td><td>Source packages</td><td class="binary" style="display:none">Binary packages</td></tr>
    <tr><td>Strong</td><td>$strong_ess_noall_src</td><td class="binary" style="display:none">$strong_ess_noall</td></tr>
    <tr><td>Opt. IS</td><td>$opt_ess_noall_src</td><td class="binary" style="display:none">$opt_ess_noall</td></tr>
    <tr><td>Closure</td><td>$closure_ess_noall_src</td><td class="binary" style="display:none">$closure_ess_noall</td></tr>
  </table>
  <h2>Bootstrap Architecture:all as well</h2>
  <table border="1">
    <tr><td>&nbsp;</td><td>Source packages</td><td class="binary" style="display:none">Binary packages</td></tr>
    <tr><td>Strong</td><td>$strong_ess_all_src</td><td class="binary" style="display:none">$strong_ess_all</td></tr>
    <tr><td>Opt. IS</td><td>$opt_ess_all_src</td><td class="binary" style="display:none">$opt_ess_all</td></tr>
    <tr><td>Closure</td><td>$closure_ess_all_src</td><td class="binary" style="display:none">$closure_ess_all</td></tr>
  </table>
  <h1><a name="downloads" />Downloadable lists</h1>
  <h2>Essential coinstallation set</h1>
  <ul>
    <li><a href="/essential/minimal-amd64.list">Essential coinstallation set</a></li>
    <li><a href="/essential/minimal-noall-src.list">Source packages (without Arch:all)</a></li>
    <li><a href="/essential/minimal-all-src.list">Source packages (with Arch:all)</a></li>
  </ul>
  <h2>Re-use existing Architecture:all and M-A:foreign</h2>
  <ul>
    <li><a href="/essential/strong-essential-noall-nomaforeign.list">Strong binary</a></li>
    <li><a href="/essential/strong-essential-noall-nomaforeign-src.list">Strong source</a></li>
    <li><a href="/essential/opt-essential-noall-nomaforeign.list">Opt. IS binary</a></li>
    <li><a href="/essential/opt-essential-noall-nomaforeign-src.list">Opt. IS source</a></li>
    <li><a href="/essential/closure-essential-noall-nomaforeign.list">Closure binary</a></li>
    <li><a href="/essential/closure-essential-noall-nomaforeign-src.list">Closure source</a></li>
  </ul>
  <h2>Re-use existing Architecture:all</h2>
  <ul>
    <li><a href="/essential/strong-essential-noall.list">Strong binary</a></li>
    <li><a href="/essential/strong-essential-noall-src.list">Strong source</a></li>
    <li><a href="/essential/opt-essential-noall.list">Opt. IS binary</a></li>
    <li><a href="/essential/opt-essential-noall-src.list">Opt. IS source</a></li>
    <li><a href="/essential/closure-essential-noall.list">Closure binary</a></li>
    <li><a href="/essential/closure-essential-noall-src.list">Closure source</a></li>
  </ul>
  <h2>Bootstrap Architecture:all as well</h2>
  <ul>
    <li><a href="/essential/closure-essential-all.list">Strong binary</a></li>
    <li><a href="/essential/closure-essential-all-src.list">Strong source</a></li>
    <li><a href="/essential/opt-essential-all.list">Opt. IS binary</a></li>
    <li><a href="/essential/opt-essential-all-src.list">Opt. IS source</a></li>
    <li><a href="/essential/strong-essential-all.list">Closure binary</a></li>
    <li><a href="/essential/strong-essential-all-src.list">Closure source</a></li>
  </ul>

  <hr />

  <p>The data used to generate this page was computed using botch, the
  bootstrap/build ordering tool chain. The source code of botch can be
  redistributed under the terms of the LGPL3+ with an OCaml linking exception.
  The source code can be retrieved from <a
  href="https://gitlab.mister-muffin.de/debian-bootstrap/botch">
  https://gitlab.mister-muffin.de/debian-bootstrap/botch</a></p>

  <p>The html page was generated by code which can be retrieved from <a
  href="https://gitlab.mister-muffin.de/josch/boott">
  https://gitlab.mister-muffin.de/josch/boott</a> and
  which can be redistributed under the terms of the AGPL3+</p>

  <p>For questions and bugreports please contact j [dot] schauer [at] email
  [dot] de.</p>

</body>
</html>
END
