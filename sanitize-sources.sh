#!/bin/sh
# this script sanitizes debian Sources files from snapshot.debian.org

date="$1"
sources="$2"

# version constraints start with a single parenthesis and like this:
# libsdl1.2-dev ((>=1.2.7+1.2.8cvs20041007-5.2)
if [ 20050814 -eq $date ]; then
	sed -i 's/libsdl1.2-dev ((>=1.2.7+1.2.8cvs20041007-5.2),/libsdl1.2-dev (>=1.2.7+1.2.8cvs20041007-5.2),/' "$sources"
fi

# version numbers cannot contain braces and dollar signs like this:
# Build-Conflicts: libxosd-dev (<< ${Source-Version})
if [ 20071028 -le $date ] && [ $date -le 20080828 ]; then
	sed -i 's/libxosd-dev (<< ${Source-Version})/libxosd-dev/' "$sources"
fi

# conflicts cannot be disjunctions like this:
# Build-Conflicts-Indep: gs-afpl | gs-gpl (= 8.01-1)
if [ 20050312 -le $date ] && [ $date -le 20071127 ]; then
	sed -i 's/Build-Conflicts-Indep: gs-afpl | gs-gpl (= 8.01-1),/Build-Conflicts-Indep: gs-afpl, gs-gpl (= 8.01-1),/' "$sources"
fi

# version numbers cannot start with a point like this:
# libvte-dev (>= .10.26) 
if [ 20050312 -le $date ] && [ $date -le 20050516 ]; then
	sed -i 's/libvte-dev (>= .10.26),/libvte-dev (>= 0.10.26),/' "$sources"
fi

# architectures are not separated by commas like this:
# expect (>= 5.38.0) [!hppa, !hurd-i386]
if [ 20050511 -le $date ] && [ $date -le 20050605 ]; then
	sed -i 's/expect (>= 5.38.0) \[!hppa, !hurd-i386\],/expect (>= 5.38.0) \[!hppa !hurd-i386\],/' "$sources"
fi

# architectures are not separated by commas like this:
# Architecture: kfreebsd-i386, i386
if [ 20050913 -le $date ] && [ $date -le 20060322 ]; then
	sed -i 's/Architecture: kfreebsd-i386, i386/Architecture: kfreebsd-i386 i386/' "$sources"
fi

# conflicts cannot be disjunctions like this:
# Build-Conflicts: ghostscript [hppa sparc] | gs [hppa sparc]
if [ 20080425 -eq $date ]; then
	sed -i 's/Build-Conflicts: ghostscript \[hppa sparc\] | gs \[hppa sparc\]/Build-Conflicts: ghostscript [hppa sparc], gs [hppa sparc]/' "$sources"
fi

# an architecture list does not contain a parenthesis in the end like this:
# [i386 m68k mips mipsel powerpc s390 alpha amd64 armel hppa ia64 ppc64 s390x sparc)]
if [ 20100410 -le $date ] && [ $date -le 20100525 ]; then
	sed -i 's/sparc)]/sparc]/' "$sources"
fi
