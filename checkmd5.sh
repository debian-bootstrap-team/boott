#!/bin/sh

file=$1
release=$2
releasepath=$3
# dscverify could be extended to support checking Release files as well
if [ "$(md5sum "$file" | awk '{print $1}')" != "$(grep-dctrl -F MD5Sum "" -s MD5Sum -n "$release" | grep " $releasepath\$" | awk '{print $1}')" ]; then
	echo "md5sum mismatch for $file"
	exit 1
fi
if [ "$(wc -c < "$file")" != "$(grep-dctrl -F MD5Sum "" -s MD5Sum -n "$release" | grep " $releasepath\$" | awk '{print $2}')" ]; then
	echo "size mismatch for $file"
	exit 1
fi
