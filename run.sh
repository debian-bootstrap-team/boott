#!/bin/bash

set -exu
set -o pipefail

trap "rm -fd ~/lockdir" EXIT

# create a lock so that this script is not inadvertently run multiple times at
# once. We use mkdir because it is atomic and will fail if the directory
# already existed.
mkdir ~/lockdir || exit 13

# boott doesn't come with SSL certificates, thus we use a wrapper around
# urllib.request which monkeypatches the urlopen function with a version
# that disables certificate checking
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
export PYTHONPATH="$SCRIPTPATH/pythonpath"

if [ -e log ]; then
	mv log log.old
fi

/usr/bin/time -v make -j2 2>&1 | tee log

htdocs="/srv/bootstrap.debian.net/htdocs/"

#for b in $(cat mainarchs.txt portarchs.txt); do
#	for h in $(cat mainarchs.txt portarchs.txt); do
#		cp -a builddebcheck-$b-$h.yaml "$htdocs"
#	done
#done

for i in co_ma_same cross_all foreign_install; do
        cp -a $i.html "$htdocs"
        cp -a $i.yaml "$htdocs"
        rsync -Pha --delete $i "$htdocs"
done

rsync -Pha --delete botch-native "$htdocs"

#for f in cross_matrix.html essential.html ma_interpreter.html; do
for f in essential.html ma_interpreter.html history.html history_cross.html history.svg history_cross.svg; do
        cp -a $f "$htdocs"
done

for t in cross closure optis optuniv disj; do
	cp -a history_$t.txt "$htdocs"
done

mkdir -p "$htdocs/webresources"
cp -a /usr/share/javascript/jquery/jquery.js "$htdocs/webresources/jquery.js"
cp -a /usr/share/javascript/jquery-tablesorter/js/jquery.tablesorter.js "$htdocs/webresources/jquery.tablesorter.js"
cp -a /usr/share/javascript/jquery-tablesorter/js/extras/jquery.tablesorter.pager.js "$htdocs/webresources/jquery.tablesorter.pager.js"
cp -a /usr/share/javascript/jquery-tablesorter/css/theme.blue.css "$htdocs/webresources/style.css"

# specify full path because /usr/local/bin is not in $PATH when script is run
# via cron
/usr/local/bin/static-update-component bootstrap.debian.net

rmdir ~/lockdir
