#!/usr/bin/env python3

import sys
from collections import defaultdict
import throw_out_your_templates as toyt
import yaml
try:
    from yaml import CBaseLoader as yamlLoader
except ImportError:
    from yaml import BaseLoader as yamlLoader

m = defaultdict(dict)

mainarchs=sys.argv[1].split()
portarchs=sys.argv[2].split()

for buildarch in mainarchs+portarchs:
    for hostarch in mainarchs+portarchs:
        with open("builddebcheck-%s-%s.yaml"%(buildarch,hostarch)) as f:
            d = yaml.load(f, Loader=yamlLoader)
            if d is None:
                print("cannot read yaml from %s" % f, file=sys.stderr)
                continue
            if d.get('output-version') != "1.2":
                print("invalid yaml in %s" % f, file=sys.stderr)
                exit(1)
            if buildarch != d['native-architecture']:
                print("unexpected build architecture", file=sys.stderr)
            if hostarch != d['host-architecture']:
                print("unexpected host architecture", file=sys.stderr)
            if d.get('source-packages') is None or d.get('broken-packages') is None:
                print("yaml incomplete: %s" % f, file=sys.stderr)
                continue
            pkgs=int(d['source-packages'])
            broken=int(d['broken-packages'])
            m[buildarch][hostarch] = broken/pkgs

def perc2color(value):
    # scale onto an exponential curve with the points (0,0) and (1,1)
    f = 5 # magic value by which to scale the exponential function
    value = (((f+1)**(1/f))**(value*f)-1)/f
    # use HSV to get a smooth transition from red to green
    blue = 0
    if 0<=value<0.5:
        green = 1.0
        red = 2*value
    else:
        red = 1.0
        green = 1.0 - 2*(value-0.5)
    return int(red*255),int(green*255),int(blue*255)

archs=sorted(mainarchs)+sorted(portarchs)

rows=[
        toyt.htmltags.tr[toyt.htmltags.td(colspan=2,rowspan=2)[""],toyt.htmltags.td(colspan=len(archs))["host architecture"],toyt.htmltags.td(rowspan=2,style="writing-mode:sideways-lr")["average"]],
        toyt.htmltags.tr[[toyt.htmltags.td(style="writing-mode:sideways-lr")[a] for a in archs]],
        ]
ha_aver=defaultdict(int)
for i,buildarch in enumerate(archs):
    cells = []
    if i == 0:
        cells.append(toyt.htmltags.td(rowspan=len(archs),style="writing-mode:sideways-lr",width="20em")["build architecture"])
    cells.append(toyt.htmltags.td()[buildarch])
    s=0
    for hostarch in archs:
        value = m[buildarch][hostarch]
        s+=value
        ha_aver[hostarch]+=value
        cells.append(toyt.htmltags.td(bgcolor="#%02X%02X%02X"%perc2color(value))[toyt.htmltags.a(href="builddebcheck-%s-%s.html" % (buildarch, hostarch))["%d"%int(value*100)]])
    aver=s/len(archs)
    cells.append(toyt.htmltags.td(bgcolor="#%02X%02X%02X"%perc2color(aver))["%d"%int(aver*100)])
    rows.append(toyt.htmltags.tr()[cells])
rows.append([toyt.htmltags.td(colspan=2)["average"]]+[toyt.htmltags.td(bgcolor="#%02X%02X%02X"%perc2color(ha_aver[a]/len(archs)))[int((ha_aver[a]*100)/len(archs))] for a in archs])

print(toyt.Serializer(toyt.xml_default_visitors_map).serialize([toyt.safe_unicode('<!DOCTYPE html>'), toyt.html(lang='en')[toyt.body[toyt.htmltags.table(border="1")[rows]]]]))
