#!/usr/bin/perl

use strict;
use warnings;

use Dpkg::Deps qw(deps_parse);
use Dpkg::Index;
use Dpkg::Control::Types;
use Dpkg::Control::FieldsCore qw(field_list_src_dep);

# does not work with old dpkg
#my $index = Dpkg::Index->new(type => CTRL_INDEX_SRC, unique_tuple_key => 1);

my $key_func = sub {return $_[0]->{Package} . '_' . $_[0]->{Version}; };
my $index = Dpkg::Index->new(type => CTRL_INDEX_SRC, get_key_func=>$key_func);

$index->load($ARGV[0]);

foreach my $key ($index->get_keys()) {
    my $cdata = $index->get_by_key($key);
    foreach my $depfield (field_list_src_dep()) {
	my $dep_line = $cdata->{$depfield};
	next if not defined($dep_line);
	$cdata->{$depfield} = deps_parse($dep_line,
	    build_dep => 1,
	    reduce_profiles => 1,
	    build_profiles => [ 'nocheck' ]);
    }
}

$index->output(*STDOUT);
